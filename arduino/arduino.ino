#include <Servo.h>

Servo s1, s2;
byte x = 90, y = 90;

void setup() {
  Serial.begin(9600);
  s1.attach(9);
  s2.attach(10);
  s1.write(90);
  s2.write(90);
}

void loop() {
  if (Serial.available() >= 3) {
    while (Serial.available() > 0 && Serial.read() != 0)
      ;

    x = Serial.read();
    y = Serial.read();

    s1.write(x);
    s2.write(y);
  }
}
